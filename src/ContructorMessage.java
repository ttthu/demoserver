/**
 * Created by ttthu on 10/9/14.
 */
public class ContructorMessage {
    private String message = null;
    /**
     * Constructor
     */
    public ConstructorMessage(String message) {
        this.message = message;
    }
    /**
     * Gets message.
     */
    public String getMessage() {
        return message;
    }

    /* *
    Sets message.
     */
    public void setMessage(String message) {
        this.message = message;
    }

}
